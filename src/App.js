// src/App.js
import React from 'react';
import './App.css';
import logo from './logo.svg';
import ComponentName from "./ComponentName";

function App() {
  return (
    <div className="App">
      <h1 className="title">W22 Hello country exercise</h1>
      <header className="App-header">
        <ComponentName country="Finland" />
      </header>
    </div>
  );
}

export default App;
